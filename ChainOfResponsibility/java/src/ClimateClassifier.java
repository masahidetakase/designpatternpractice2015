// ケッペンの気候区分の判定のchain（２分枝）
public abstract class ClimateClassifier
{	
	ClimateClassifier positive_next; // 基準に合致した場合の次の処理
	
	ClimateClassifier negative_next; // 基準に合致しなかった場合の次の処理
	
	// 連鎖的に実行されるメソッド
	public void
	execute( Climate climate, ClimateClass division)
	{
		// climateを分析し、基準に合致すればdivisionのメンバを書き換える。
		boolean result = detect( climate, division);
		
		// 基準に合致し、かつ次の基準がある
		if( result && positive_next != null) {
			positive_next.execute( climate, division);
		}
		else if( !result && negative_next != null) {
			// 基準に合致せず、かつ次の基準がある
			negative_next.execute( climate, division);
		}
	}
	
	/* 判定条件のTemplate method.
	 *  climateを解析し、基準に合致していれば真、合致しなければ偽を返す。
	 *  必要であればdivisionのメンバを書き換える。
	 */
	abstract protected  boolean
	detect( Climate climate, ClimateClass division);
	
	// 基準に合致した場合の分岐先を設定
	public ClimateClassifier
	positive( ClimateClassifier next)
	{
		// 循環参照を禁止
		if( this == next) { 
			throw new IllegalArgumentException( "Circular reference.");
		}
		
		this.positive_next = next;
		
		return this.positive_next;
	}
	
	// 基準に合致しなかった場合の分岐先を設定
	public ClimateClassifier
	negative( ClimateClassifier next)
	{
		// 循環参照を禁止
		if( this == next) { 
			throw new IllegalArgumentException( "Circular reference.");
		}
		
		this.negative_next = next;
		
		return this.negative_next;
	}
	
	// ユーティリティメソッド -------------------------------------
	
	// 気候区（乾期パターン）の判定
	static protected ClimateClass.MinorClass
	detectMinorClass( Climate climate)
	{
		float rf_max_year = climate.getMaxRainFall();
		float rf_min_year = climate.getMinRainFall();
		float rf_max_summer = climate.getMaxSummerRainfall();
		float rf_max_winter = climate.getMaxWinterRainfall();
		
		if( ( rf_max_year == rf_max_summer) &&
		    ( rf_min_year * 10.f < rf_max_year)) {
			return ClimateClass.MinorClass.DryWinter;
		} else if( ( rf_max_year == rf_max_winter) &&
				  ( rf_min_year * 3.f < rf_max_year) &&
				  ( rf_min_year < 30.f)) {
			return ClimateClass.MinorClass.DrySummer;
		} else {
			return ClimateClass.MinorClass.Moist;
		} 
	}
	
	// 乾燥限界（年推定蒸発量）の計算式
	static protected float
	calcAridBoundary( Climate climate)
	{		
		float r = 0.f;
		float averageTemp = climate.getAirTempAverage();
		ClimateClass.MinorClass minor = detectMinorClass( climate);		
		
		if( minor == ClimateClass.MinorClass.DrySummer) {
			r = 20.f * averageTemp; 
		} else if( minor == ClimateClass.MinorClass.Moist) {
			r = 20.f * ( averageTemp + 7.f);
		} else if( minor == ClimateClass.MinorClass.DryWinter) {
			r = 20.f * ( averageTemp + 14.f);
		} else {
			throw new InternalError( "Invalid minor climate class.");
		}

		return r;
	}
	
}



