//観測地点の情報
public class Location
{
	private String name = null;

	private float longitude_deg = Float.NaN; // 緯度（度）。正：北緯、負：南緯

	private float latitude_deg = Float.NaN; // 経度（度）。正：東経、負：西経

	public
	Location() {}
	
	public
	Location( String name, // 地名
			  float longitude_deg, // 緯度(正：北緯、負：南緯)
			  float latitude_deg) // 経度(正：東経、負：西経)
	{
		assert( name != null);
		assert( ( -180.f <= longitude_deg) && ( longitude_deg <= 180.f));
		assert( ( -180.f <= latitude_deg) && ( latitude_deg <= 180.f));
		
		this.name = name;
		this.longitude_deg = longitude_deg;
		this.latitude_deg = latitude_deg;
	}

	@Override
	public String toString()
	{
		if( name == null) {
			return "nowhere";
		}
		return String.format( "%s (%.2f,%.2f)",
							  name,
							  longitude_deg,
							  latitude_deg);
	}
	
	String name() { return name;}
	
	public float
	longitude() { return longitude_deg;}
	
	public float
	latitude() { return latitude_deg;}
}
