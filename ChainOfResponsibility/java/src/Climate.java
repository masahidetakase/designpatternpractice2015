import java.util.ArrayList;
import java.util.Collections;

// 気候（月平均気温と降水量）を格納する
public class Climate
{

	ArrayList<Float> monthly_rainfall_mm = new ArrayList<Float>(12); // 月降水量

	ArrayList<Float> monthly_air_temp_deg = new ArrayList<Float>(12); // 月平均気温

	static int[] Northern_Summer_Monthes = { 6, 7, 8};
	static int[] Southern_Summer_Monthes = { 12, 1, 2};

	static int[] Northern_Winter_Monthes = { 12, 1, 2};
	static int[] Southern_Winter_Monthes = { 6, 7, 8};

	int[] summer_month;
	int[] winter_month;
	
	public Climate()
	{
		for( int i = 0; i < 12; ++i) {
			monthly_air_temp_deg.add( 0.f);
			monthly_rainfall_mm.add( 0.f);
		}
	}

	public
	Climate( float[] air_temp, // 月平均気温
			 float[] rainfall_mm, // 降水量
			 boolean in_northan_hemisphere) // 北半球か？
	{
		assert( air_temp.length == 12);
		assert( rainfall_mm.length == 12);

		for( int i = 0; i < 12; ++i) {
			monthly_air_temp_deg.add( air_temp[i]);
			monthly_rainfall_mm.add( rainfall_mm[i]);
		}

		// 北半球？
		if( in_northan_hemisphere) {
			summer_month = Northern_Summer_Monthes;
			winter_month = Northern_Winter_Monthes;
		} else { // 南半球？
			summer_month = Southern_Summer_Monthes;
			winter_month = Southern_Winter_Monthes;
		}
	}
	
	@Override
	public String toString()
	{
		String ret = summer_month == Northern_Summer_Monthes ?
						"Northan hemisphere" : "Southern hemisphere";
		
		ret += System.lineSeparator();
		
		for( int i = 0; i < 12; ++i) {
			ret += String.format( "Month:%02d   %1.2f deg.  %1.2f mm",
					i+1,
					monthly_air_temp_deg.get(i),
					monthly_rainfall_mm.get(i));
			
			ret += System.lineSeparator();
		}
				
		return ret;
	}

	// 気温を取得
	public float
	getAirTemp( int month)
	{
		assert( ( 0 < month) && ( month <= 12));
		
		return monthly_air_temp_deg.get( month);
	}
	
	// 気温を取得
	public float
	getRainfall( int month)
	{
		assert( ( 0 < month) && ( month <= 12));
		
		return monthly_rainfall_mm.get( month);
	}

	// 平均降水量(mm/month)を求める
	public float
	getRainfallAverage()
	{
		float average = 0.f;
		for( Float rainfall : monthly_rainfall_mm) {
			average += rainfall;
		}

		return average / 12.f;
	}

	// 最少降水量
	public float
	getMinRainFall()
	{
		return Collections.min( monthly_rainfall_mm);
	}

	// 最大降水量
	public float
	getMaxRainFall()
	{
		return Collections.max( monthly_rainfall_mm);
	}

	// 夏季最大降水量
	public float
	getMaxSummerRainfall()
	{
		float rainfall = 0.f;

		for( int i : summer_month) {
			rainfall = Math.max( rainfall, monthly_rainfall_mm.get(i-1));
		}

		return rainfall;
	}

	// 冬季最大降水量
	public float
	getMaxWinterRainfall()
	{
		float rainfall = 0.f;

		for( int i : winter_month) {
			rainfall = Math.max( rainfall, monthly_rainfall_mm.get(i-1));
		}

		return rainfall;
	}

	// 年平均気温を求める
	public float
	getAirTempAverage()
	{
		float average = 0.f;
		for( Float temp : monthly_air_temp_deg) {
			average += temp;
		}

		return average / 12.f;
	}

	// 年降水量
	public float
	getTotalRainfallYear()
	{
		float rf_year = 0.f;

		for( Float rf : monthly_rainfall_mm) {
			rf_year += rf;
		}

		return rf_year;
	}

	// 最寒月の平均気温
	public float
	getMinAirTemp()
	{
		return Collections.min( monthly_air_temp_deg);
	}

	// 最暑月の平均気温
	public float
	getMaxAirTemp()
	{
		return Collections.max( monthly_air_temp_deg);
	}

	// 範囲内の気温の月の数( min <= temp < max)
	public int
	getNumOfMonthInTemp( float min_temp, float max_temp)
	{
		assert( min_temp < max_temp);

		int num_month = 0;

		for( Float temp : monthly_air_temp_deg) {
			if( ( min_temp <= temp) && ( temp < max_temp)) {
				++num_month;
			}
		}

		return num_month;
	}
}
