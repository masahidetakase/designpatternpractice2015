// 温帯の分類
public class TemperateZone extends ClimateClassifier
{
	public TemperateZone()
	{
		super();
		
		// TODO： ここでチェインを連結させる
	}

	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( climate.getMinAirTemp() < 18.f); // 熱帯と温帯との境界
		
		// 最寒月平均気温が-3℃以上なら温帯
		if( -3.f <= climate.getMinAirTemp()) {
			division.major = ClimateClass.MajorClass.Temperate;
			
			// 乾期のパターンを判定
			division.minor = detectMinorClass( climate);
			
			return true;
		}
		
		return false;
	}
}

// 気候区は湿潤型(f)か？
class IsMoistType extends ClimateClassifier
{
	public IsMoistType() { super();}

	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( division.major == ClimateClass.MajorClass.Temperate);
		
		division.subminor = ClimateClass.SubMinorClass.None;
		
		if( division.minor == ClimateClass.MinorClass.Moist) {
			return true;
		}
		
		return false;
	}	
}

// 西岸海洋性気候か？
class IsMarineWestCoast extends ClimateClassifier
{
	public IsMarineWestCoast() { super();}

	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( division.major == ClimateClass.MajorClass.Temperate);
		assert( division.minor == ClimateClass.MinorClass.Moist);
		
		// TODO: 気候区（minor class）が湿潤（f）かつ最暖月気温が22度以上なら西岸海洋性気候
		return false;
	}
}

// 温暖湿潤気候
class HumidSubtropical extends ClimateClassifier
{
	public HumidSubtropical() { super();}

	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( division.major == ClimateClass.MajorClass.Temperate);
		assert( division.minor == ClimateClass.MinorClass.Moist);
		
		// TODO: 気候区（minor class）が湿潤（f）かつ最暖月気温が22度未満なら温暖湿潤気候
		return false;
	}
}