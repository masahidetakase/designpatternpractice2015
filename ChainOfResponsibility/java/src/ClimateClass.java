// ケッペン気候区分
public class ClimateClass
{
	// 気候帯
	enum MajorClass
	{
		None( ""), // 未判定
		Tropic( "A"), // 熱帯
		Drying( "B"), // 乾燥帯
		Temperate( "C"), // 温帯
		Subarctic( "D"), // 亜寒帯
		Frigid( "E"); // 寒帯

		String mark;

		MajorClass( String mark) { this.mark = mark;}

		@Override
		public String toString() { return mark;}
	}

	// 気候区
	enum MinorClass
	{
		None( ""), // 未判定
		Moist( "f"), // 湿潤
		Monsoon( "m"), // モンスーン
		DryWinter( "w"), // 冬季乾燥
		DrySummer( "s"), // 夏季乾燥
		Desert	( "W"), // 砂漠
		Steppe( "S"), // ステップ
		Tundra( "T"), // ツンドラ
		Nival( "F"); // 氷雪

		String mark;

		MinorClass( String mark) { this.mark = mark;}

		@Override
		public String toString() { return mark;}
	}

	//温帯（C）、亜寒帯(D）の細分
	enum SubMinorClass
	{
		None( ""), // 未判定または無し
		a( "a"), // 最暖月が10℃以上22℃未満 かつ 月平均気温10℃以上の月が4か月以上
		b( "b"), // 最暖月が10℃以上22℃未満 かつ 月平均気温10℃以上の月が3か月以下
		c( "c"), // 最暖月が10℃以上22℃未満 かつ 月平均気温10℃以上の月が3か月以下かつ最寒月が-38℃以上-3℃未満
		d( "W"); // 最暖月が10℃以上22℃未満 かつ 月平均気温10℃以上の月が3か月以下かつ最寒月が-38℃未満

		String mark;

		SubMinorClass( String mark) { this.mark = mark;}

		@Override
		public String toString() { return mark;}
	}

	// Enumのみからなるエンティティクラスなので、setter/getterは必要ないと判断した
	public MajorClass major; // 大区分

	public MinorClass minor; // 小区分

	public SubMinorClass subminor; // 細区分

	public ClimateClass()
	{
		major = MajorClass.None;
		minor = MinorClass.None;
		subminor = SubMinorClass.None;
	}

	@Override
	public String toString()
	{
		return major.toString() + minor + subminor;
	}
}
