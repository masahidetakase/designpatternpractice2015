// 乾燥帯の分類
public class DesertZone extends ClimateClassifier
{
	public
	DesertZone()
	{
		super();
		// 乾燥帯で、ステップ気候でなければ、砂漠気候である
		this.positive( new IsSteppe()).negative( new Desert());
	}

	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		float r = calcAridBoundary( climate); // 乾燥限界
		float rf_year = climate.getTotalRainfallYear(); // 年間降水量
		
		if( r > rf_year) { // 乾燥限界が年間降水量を上回れば乾燥帯
			division.major = ClimateClass.MajorClass.Drying;
			return true;
		} 
		
		return false;
	}
}

// ステップ気候か？
class IsSteppe extends ClimateClassifier
{
	public IsSteppe() { super();}
	
	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( division.major == ClimateClass.MajorClass.Drying);
		
		float r = calcAridBoundary( climate); // 乾燥限界
		float rf_year = climate.getTotalRainfallYear(); // 年間降水量
		
		if( 0.5f * r <= rf_year) {
			division.minor = ClimateClass.MinorClass.Steppe;
			division.subminor = ClimateClass.SubMinorClass.None;
			return true;
		}
		
		return false;
	}
}

// 砂漠気候
class Desert extends ClimateClassifier
{
	public Desert() { super();}
	
	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( division.major == ClimateClass.MajorClass.Drying);

		division.minor = ClimateClass.MinorClass.Desert;
		division.subminor = ClimateClass.SubMinorClass.None;
		
		return true;
	}
}
