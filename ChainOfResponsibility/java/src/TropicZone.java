// 熱帯気候の分類
public class TropicZone extends ClimateClassifier
{
	public TropicZone()
	{ 
		super();
		
		// 熱帯で、熱帯雨林気候でなければ、サバナ気候。サバナ気候でなければ熱帯モンスーン気候
		this.positive( new IsTropicalRainForest()).
			negative( new IsSavannah()).
			negative( new TropicalMonsoon());
	}

	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		if( 18.f <= climate.getMinAirTemp()) {
			division.major = ClimateClass.MajorClass.Tropic;
			return true;
		}
		
		return false;
	}
}

// 熱帯雨林気候か？
class IsTropicalRainForest  extends ClimateClassifier
{
	public IsTropicalRainForest()
	{ 
		super();
	}

	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( division.major == ClimateClass.MajorClass.Tropic);
		
		if( 60.f <= climate.getMinRainFall()) {
			division.minor = ClimateClass.MinorClass.Moist;
			division.subminor = ClimateClass.SubMinorClass.None;
			
			return true;
		}
		
		return false;
	}
}

// サバナ気候か？
class IsSavannah extends ClimateClassifier
{
	public IsSavannah()
	{ 
		super();
	}

	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( division.major == ClimateClass.MajorClass.Tropic);		
		
		float rf_year = climate.getTotalRainfallYear();
		float rf_min = climate.getMinRainFall();
		
		if( ( rf_year <= 2500.f) &&
		    ( rf_min < borderFunc( rf_year))) {
			division.minor = ClimateClass.MinorClass.DryWinter;
			division.subminor = ClimateClass.SubMinorClass.None;
			
			return true;
		}
		
		return false;
	}
	
	// 最少雨月降水量―年間降水量のグラフにおけるサバンナ気候と熱帯モンスーン気候との境界
	float
	borderFunc( float rf_year)
	{
		assert( rf_year <= 2500.f);
		
		if( rf_year <= 1000.f) {
			return 60.f;
		} else if( ( 1000.f < rf_year) && ( rf_year < 2500.f)) {
			return -60.f/1500.f * ( rf_year - 1000.f) + 60.f;
		} else {
			return 0.f;
		}
	}
}

// 熱帯モンスーン気候
class TropicalMonsoon extends ClimateClassifier
{
	public TropicalMonsoon()
	{ 
		super();
	}

	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( division.major == ClimateClass.MajorClass.Tropic);		
		
		division.minor = ClimateClass.MinorClass.Monsoon;
		division.subminor = ClimateClass.SubMinorClass.None;
		
		return true;
	}
}