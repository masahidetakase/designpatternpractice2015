import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Main {

	public static void main(String[] args)
	{
		ClimateClassifier isfrigidZone = new FrigidZone();

		ClimateClassifier isDesertZone = new DesertZone();

		ClimateClassifier isTropicZone = new TropicZone();

		ClimateClassifier isTemperateZone = new TemperateZone();

		ClimateClassifier isSubarcticZone = new SubarcticZone();

		ClimateClassifier climate_detector = isfrigidZone; // 最初の分類は"寒帯か？"

		// 各気候区判定チェインを連結
		climate_detector.
			negative( isDesertZone).
			negative( isTropicZone).
			negative( isTemperateZone).
			negative( isSubarcticZone);


		// 気候データをファイルから読み込み、表示する
		String data_file_path = chooseCsvFile();

		if( data_file_path == null) { return ;}

		Climate climate = createClimate( data_file_path);

		System.out.print( climate);

		ClimateClass division = new ClimateClass();

		// 判定を実行する
		climate_detector.execute( climate, division);

		// 気候区分を表示する
		System.out.println( "Climate type: " + division);
	}

	/* 気候データのファイルを読み込む
	 * # 都市名 (緯度,軽度)
	 * 1, 2.2,  11  (1月の気温、降水量)
	 * 2, 3.5,  20  (2月の気温、降水量)
	 * :   :     :
	 * :   :     :
	 * 12, 5.1, 22 （12月の気温、降水量）
	 */
	static Climate
	createClimate( String file_path) // CSVファイルパス
	{
		try {
			 BufferedReader reader =
				new BufferedReader( new FileReader( file_path));

			 String header = reader.readLine().trim(); // 先頭行
			 Location loc = readLocation( header);

			 float[] temperature = new float[12];
			 float[] rainfall = new float[12];

			 String line;
			 int month_count = 0;
			 while( ( ( line = reader.readLine()) != null) &&
					( month_count < 12)) {

				 float[] vals = readMonthlyClimate( line);

				 if( vals != null) {
					 temperature[month_count] = vals[1];
					 rainfall[month_count] = vals[2];
					 ++month_count;
				 }
			 }

			 reader.close();

			 return new Climate( temperature, rainfall, loc.longitude() >= 0.f);

		} catch( IOException ex) {
			return null;
		}
	}

	// ヘッダから観測地点の情報を取得
	static Location
	readLocation( String header)
	{
		String regex = "^#\\s*(\\w+)\\s*\\(\\s*([0-9\\.]+)\\s*,\\s*([0-9\\.]+)\\s*\\)\\s*$";

		Pattern pattern = Pattern.compile( regex);

		Matcher matcher = pattern.matcher( header);

		if( !matcher.find()) { return null;}

		String matchstr = matcher.group();

		int num_groups = matcher.groupCount();

		if( num_groups != 3) {
			return null;
		}

		Location loc = new Location( matcher.group(1),
									 Float.parseFloat( matcher.group(2)),
									 Float.parseFloat( matcher.group(3)));

		return loc;
	}

	// 行から気温（度）と降水量（mm）を読み取る
	static float[]
	readMonthlyClimate( String line)
	{
		String[] val_strs = line.trim().split( ",");

		if( val_strs.length != 3) {
			return null;
		}

		float vals[] = { Float.parseFloat( val_strs[0]),
						 Float.parseFloat( val_strs[1]),
				         Float.parseFloat( val_strs[2])};
		return vals;
	}

	static String
	chooseCsvFile() {
		JFileChooser csv_chooser = new JFileChooser( System.getProperty("user.dir"));

		FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV files", "csv");

		csv_chooser.setFileFilter( filter);

		int ret = csv_chooser.showOpenDialog(null);

		if(ret == JFileChooser.APPROVE_OPTION) {
			return csv_chooser.getSelectedFile().getAbsolutePath();
		} else {
			return null;
		}

	}
}
