// 亜寒帯の分類
public class SubarcticZone extends ClimateClassifier
{
	public SubarcticZone()
	{
		super();
		
		this.positive( new IsDryWinter()).negative( new MoistSubarctic());
	}

	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( climate.getMinAirTemp() < -3.f);
		
		division.major = ClimateClass.MajorClass.Subarctic;
		division.minor = ClimateClass.MinorClass.None;
		division.subminor = ClimateClass.SubMinorClass.None;
		
		return true;
	}
}

// 冷帯冬季少雨気候か？
class IsDryWinter extends ClimateClassifier
{
	public IsDryWinter() { super();}
	
	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( division.major == ClimateClass.MajorClass.Subarctic);
		
		division.minor = detectMinorClass( climate);
		
		return division.minor == ClimateClass.MinorClass.DryWinter;
	}
}

// 冷帯湿潤気候
class MoistSubarctic extends ClimateClassifier
{
	public MoistSubarctic() { super();}
	
	@Override
	protected boolean
	detect(Climate climate, ClimateClass division)
	{
		assert( division.major == ClimateClass.MajorClass.Subarctic);
		
		division.minor = ClimateClass.MinorClass.Moist;
		
		return true;
	}
}