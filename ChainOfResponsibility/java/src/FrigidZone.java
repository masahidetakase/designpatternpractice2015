// 寒帯の分類のchain
public class FrigidZone extends ClimateClassifier
{
	public FrigidZone()
	{
		super();
		
		/* 寒帯 --yes--> ツンドラ？ --yes--> End
		 *                  |
		 *                +--no--> 氷雪 
		 */
		this.positive( new IsTundra()).negative( new Nival());
	}
	
	@Override
	protected boolean
	detect( Climate climate,
			 ClimateClass division)
	{
		if( climate.getAirTempAverage() < 10.f) {
			division.major = ClimateClass.MajorClass.Frigid;
			return true;
		}
		
		return false;
	}
	
	// ツンドラ気候か？
	class IsTundra extends ClimateClassifier
	{
		public IsTundra() { super();}
		
		@Override
		protected boolean
		detect(Climate climate, ClimateClass division)
		{
			assert( division.major == ClimateClass.MajorClass.Frigid);
			
			if( climate.getMaxAirTemp() < 0.f) {
				division.minor = ClimateClass.MinorClass.Tundra;
			}
			return false;
		}
	}
	
	// 氷雪気候？
	class Nival extends ClimateClassifier
	{
		
		public Nival() { super();}

		@Override
		protected boolean
		detect(Climate climate, ClimateClass division)
		{
			assert( division.major == ClimateClass.MajorClass.Frigid);
			
			division.minor = ClimateClass.MinorClass.Nival;
			
			return true;
		}
	}
}
