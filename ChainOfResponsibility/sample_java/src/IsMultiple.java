// 倍数か否か判定する
public class IsMultiple extends IntegerFilterChain
{
	int div_num;
	
	public 
	IsMultiple( int div_num)
	{
		assert( 0 < div_num);
		this.div_num = div_num;
	}
	
	
	@Override
	protected boolean
	execute(int val)
	{
		System.out.println( "    " +
							val + 
							" is a multiple of " +
							div_num +
							".");
		
		return val % div_num == 0;
	}

}
