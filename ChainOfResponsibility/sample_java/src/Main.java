import java.io.*;

public class Main {

	public static void main(String[] args)
	{
		// 先頭のフィルタ
		IntegerFilterChain filter_chains = new IsPrime();
		
		// フィルタを連結する
		filter_chains.
			next( new IsMultiple(5)).
			next( new IsMultiple(11)).
			next( new HasRemainder( 3, 2));
		
		BufferedReader console_in =
			new BufferedReader( new InputStreamReader( System.in));
			
		while( true) {
			
			System.out.print("自然数を入力してください : ");
			
			String input_str;
			
			try {
				// コンソールから1行読み込む
				input_str = console_in.readLine();
				
				if( input_str == null) {
					break;
				} else {
					input_str = input_str.trim();
					
					// 空文字で終了
					if( input_str.isEmpty()) { break;}
				}
			
			} catch (Exception e) {
				break;
			}
			
			// 入力された数字を判定する。
			boolean result = filter_chains.check( Integer.parseInt( input_str));
				
			System.out.println( "結果:" + result);
		}
	}

}
