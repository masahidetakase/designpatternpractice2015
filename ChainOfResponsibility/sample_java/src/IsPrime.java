// 素数か否かを判定する
public class IsPrime extends IntegerFilterChain
{
	@Override
	protected boolean
	execute(int val)
	{
		System.out.println( "    " +
							val +
							" is a prime num. ?");
		
		if( val <= 1) { return false;} // 1は素数ではない
		
		for( int div_num = (int)Math.sqrt( val); 1 < div_num; --div_num) {
			if( val % div_num == 0) {
				return false;
			}
		}
		
		return true;
	}
}
