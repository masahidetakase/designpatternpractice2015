// 剰余が
public class HasRemainder extends IntegerFilterChain
{
	int div_num; // 割る数
	
	int remainder; // 余り
	
	public
	HasRemainder( int div_num, int remainder)
	{
		assert( 0 < div_num);
		assert( 0 <= remainder);
		
		this.div_num = div_num;
		this.remainder = remainder;
	}
	
	@Override
	protected boolean
	execute( int val)
	{
		System.out.println( "    " +
							val + 
							" mod " +
							div_num +
							" equals to " +
							remainder +
							" ?");
		
		return val % div_num == remainder;
	}

}
