// 整数のフィルタの基底クラス
public abstract class IntegerFilterChain
{
	IntegerFilterChain next = null; // 次のフィルタへの参照
	
	// フィルタを連結し、連結したフィルタ自身を返す。
	public IntegerFilterChain
	next( IntegerFilterChain next)
	{
		// 循環参照を禁止
		if( this == next) { 
			throw new IllegalArgumentException( "Circular reference.");
		}
		
		this.next = next;
		
		return next;
	}
	
	/* 判定を実行し、結果が真であれば真を、そうでなければ
	 *  次のフィルタを実行する。
	 *  次が存在しなければ偽を返す。
	 */ 
	public boolean
	check( int val)
	{
		if( !execute( val)) { // 判定結果が偽なら
			// 次のフィルタがあれば、それで判定する。
			if( next != null) {
				return next.check( val);
			} else {
				return false;
			}
		}
		
		return true;
	}
	
	// 整数の判定条件。派生クラスで実装する。
	abstract protected boolean
	execute( int val);
}
