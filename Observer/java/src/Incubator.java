import java.util.Timer;
import java.util.TimerTask;

// 恒温器のクラス
public class Incubator extends ModelIF
{
	// 恒温器の温度の上限
	float temperature_upper_thrd_deg;

	boolean is_thermostat_on; // サーモスタットの状態

	// 一秒ごとに呼ばれるタイマー
	Timer timer;

	// タイマータスク
	ThermoSensor thermo_sensor;

	public
	Incubator( float init_temp_deg, // 初期温度
			   float target_temp_deg, // 設定温度
			   float upper_temp_thrd_deg) // 上限温度
	{
		assert( target_temp_deg + 1 < upper_temp_thrd_deg);

		this.temperature_upper_thrd_deg = upper_temp_thrd_deg;
		this.timer = new Timer( true);
		this.thermo_sensor = new ThermoSensor( init_temp_deg,
										       target_temp_deg) ;

		// 1秒おきに温度を更新
		timer.schedule( thermo_sensor, 0, 1000);
	}


	// 温度を取得する
	public float
	getTemp() { return thermo_sensor.get();}
	
	// 警告を発する温度を取得
	public float
	getAlertTemp() { return temperature_upper_thrd_deg;}

	// 温度をチェックする。もし、上限温度より高ければ、Observerに通知する
	public void
	checkTemp()
	{
		float cur_temp = thermo_sensor.get();

		// Observerに通知する
		if( temperature_upper_thrd_deg < cur_temp) {
			System.out.println( "Event: WARNING! Overheat!");
			notifyObservers();
		}
	}

	// サーモスタットを有効・無効化する。
	public void
	setThermostatOn( boolean is_on) // 真：On, 偽：Off
	{
		if( is_on) {
			System.out.println( "Event: The thermostat turns on.");
			thermo_sensor.turnOnThermoStat();}
		else {
			System.out.println( "Event: The thermostat turns off.");
			thermo_sensor.turnOffThermoStat();;
		}
	}

	// サーモスタットは有効か？
	public boolean
	isThermostatOn() { return thermo_sensor.isThermoStatOn();}
}

// 温度センサーのシミュレータ
class ThermoSensor extends TimerTask
{
	float current_temp_deg; // 現在温度

	float target_temp_deg; // 目標温度

	boolean is_thermostat_on; // サーモスタットの状態

	// 故障時の温度上昇率(deg/s)
	static final float Temp_Increase_Rate = 1.5f;

	// 温度変化率（deg/s）
	static final float Temp_Control_Rate = 0.8f;

	// 庫温の上限（deg）
	static final float Max_Temperature = 85.f;

	public
	ThermoSensor( float init_temp_deg, // 初期温度
				  float target_temp_deg) // 設定温度
	{
		assert( init_temp_deg < Max_Temperature);

		this.current_temp_deg = init_temp_deg;
		this.target_temp_deg = target_temp_deg;
		this.is_thermostat_on = true;
	}

	// 1 sec 毎に呼び出される
	@Override
	public void run()
	{
		if( !is_thermostat_on) {
			// サーモスタットが切られていれば庫温は上昇する
			current_temp_deg += Temp_Increase_Rate;

			if( Max_Temperature < current_temp_deg) {
				current_temp_deg = Max_Temperature;
			}
		} else {
			// 庫内が設定温度より高ければ下げ、低ければ上げる。
			current_temp_deg +=
				Math.signum( target_temp_deg - current_temp_deg) *
				Temp_Control_Rate;
		}

		// 本物らしく見せるためのノイズ
		current_temp_deg += (Math.random() - 0.5) * 1.2;
	}

	// サーモスタットを有効にする
	public synchronized void
	turnOnThermoStat()
	{
		is_thermostat_on = true;
	}

	// サーモスタットを無効にする
	public synchronized void
	turnOffThermoStat()
	{
		is_thermostat_on = false;
	}

	// 温度を取得
	public float
	get()
	{
		float temperature;
		synchronized ( this) {
			temperature = this.current_temp_deg;
		}
		return temperature;
	}

	// サーモスタットの状態を取得
	public boolean
	isThermoStatOn()
	{
		boolean thermostat_state;
		
		synchronized ( this) {
			thermostat_state = this.is_thermostat_on;
		}
		return thermostat_state;
	}
}
