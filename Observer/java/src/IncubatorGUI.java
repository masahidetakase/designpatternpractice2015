// IncubatorのGUI。
/* 培養器のエミュレータであり、本筋とは関係はないので
 * 無視して良い。
 * 遅延初期化型SingletonおよびSwingのListenerの使い方の例
 * としてなら、参考になるかもしれない。
 */

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;
import javax.swing.event.MouseInputAdapter;

public class IncubatorGUI extends JFrame
{
	private static final long serialVersionUID = 1L;
	
	// Singletonインスタンス
	static IncubatorGUI instance = null; 
	
	// タイトル
	static final String Window_Title = "Plant Incubator";

	// サーモスタット有効状態の画像名
	static final String Thermostat_On_Image_Name = "./incubator_thermostat_on.png";
	
	// サーモスタット無効状態の画像名
	static final String Thermostat_Off_Image_Name = "./incubator_thermostat_off.png"; 
	
	Incubator incubator; // 恒温器（モデル）への参照
	
	ImageIcon thermostat_on_icon; // サーモスタット有効状態の画像
	
	ImageIcon thermostat_off_icon; // サーモスタット無効状態の画像

	JLabel image_label; // インキュベーターの画像を表示
	
	JLabel temp_indicator; // インキュベーターの温度を表示
		
	Timer monitor_timer; // インキュベーターの状態チェック用タイマ
	
	Timer indicator_timer; // 温度表示の更新用タイマ
	
	/* Singletonインスタンスの生成・取得。
	 * 一度引数を与えて生成すれば、それ以降は引数をnullにして
	 * インスタンスを取得する。
	 */
	public static synchronized IncubatorGUI
	getInstance( Incubator incubator)
	{
		if( instance == null) {
			assert( incubator != null); 
			instance = new IncubatorGUI( incubator);
		}
		
		return instance;
	}
	
	private
	IncubatorGUI( Incubator incubator)
	{
		super.setTitle( Window_Title);
		
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.incubator = incubator;
	
		// 画像をロード
		thermostat_on_icon =
			new ImageIcon( this.getClass().getResource( Thermostat_On_Image_Name));
			
		thermostat_off_icon =
			new ImageIcon( this.getClass().getResource( Thermostat_Off_Image_Name));
		
		image_label = new JLabel();
		
		temp_indicator = new JLabel();
		
		showDoorState();
		updateTempIndicator();
		
		setLayout( new BoxLayout( getContentPane(), BoxLayout.Y_AXIS));

		add( image_label);
		add( temp_indicator);
		
		initEventHandler();
		
		setMonitoringTimer();
		
		pack();
	}
	
	// インキュベーターの異常を定期的にチェックする。
	void
	setMonitoringTimer()
	{
		// 温度表示
		ActionListener monitoring_action =
			new ActionListener() {
				@Override
				public void
				actionPerformed(ActionEvent e)
				{
					incubator.checkTemp();
				}
			};
			
		// 2秒毎に温度表示を更新
		monitor_timer = new Timer( 2000, monitoring_action);
		monitor_timer.start();
	}
	
	void
	initEventHandler()
	{
		assert( image_label != null);
		assert( temp_indicator != null);
		
		// マウスを左クリックすると画像が変わる*/
		image_label.addMouseListener(
			new MouseInputAdapter() {
				@Override
				public void mouseClicked(MouseEvent e)
				{
					if( e.getButton() == MouseEvent.BUTTON1)
					{
						incubator.setThermostatOn( !incubator.isThermostatOn());
						showDoorState();
					}
				}
			});
		
		// 温度表示
		ActionListener indicator_update_action =
			new ActionListener() {
				@Override
				public void
				actionPerformed(ActionEvent e)
				{
					updateTempIndicator();
				}
			};
		
		// 1秒毎に温度表示を更新
		indicator_timer = new Timer( 1000, indicator_update_action);
		indicator_timer.start();
	}
	
	private void
	showDoorState()
	{
		if( incubator.isThermostatOn()) {
			image_label.setIcon( thermostat_on_icon);
		} else {
			image_label.setIcon( thermostat_off_icon);
		}
	}
	
	private void
	updateTempIndicator()
	{
		temp_indicator.setText( String.format( "%03.1f ℃", incubator.getTemp()));
	}
}
