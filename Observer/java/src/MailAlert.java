﻿import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailAlert implements ObserverIF
{
	Incubator incubator;

	MailSender mail_sender;

	int invok_counter = 0;

	String to_address;
	
	static public final String Ricoh_SMTP_Server = "mail.ricoh.co.jp";

	public
	MailAlert( ModelIF model, // モデルへの参照
			   String smtp_address, // SMTPサーバー
			   String from_address, // 送信者のメールアドレス
			   String to_address) // 宛先のメールアドレス
	{
		this.incubator = (Incubator)model;

		mail_sender = new MailSender( smtp_address, from_address);

		this.to_address = to_address;
	}

	@Override
	public void update()
	{
		// メール爆弾にならないように5 回に1回送信
		if( invok_counter % 5 == 0) {
			mail_sender.sendMesg( to_address,
					              "ALERT! Incubator is Overheat",
					              "Temp:" + incubator.getTemp() + "Help me!");
			invok_counter = 0;
		}
	}

}

// メールを送信するクラス
class MailSender
{
	String smtp_addr;

	String from_addr;

	// メールサーバーと送信者のアドレス
	public
	MailSender( String smtp_addr, String from_addr)
	{
		this.smtp_addr = smtp_addr;
		this.from_addr = from_addr;
	}

	// 送信
	public void
	sendMesg( String to_addr, // あて先
			  String subject, // 件名
			  String mesg) // 本文
	{

		Properties props = new Properties();
		
		props.put("mail.smtp.host", smtp_addr);
		props.put("mail.host", smtp_addr);
		props.put("mail.from", from_addr);

		Session session = Session.getInstance( props, null);

		try {
			Message mail_msg = new MimeMessage( session);
			mail_msg.setFrom( new InternetAddress( from_addr));

			InternetAddress[] address = { new InternetAddress( to_addr)};
			mail_msg.setRecipients( Message.RecipientType.TO, address);
			mail_msg.setSubject( subject);
			mail_msg.setSentDate( new Date());
		    mail_msg.setText( mesg);

		    Transport.send( mail_msg);
		} catch(MessagingException mex){
			System.out.println( "Something wrong");
		}
	}
}
