// 監視者クラスのIF
public interface ObserverIF
{
	// 監視対象の状態が変更されると、呼び出される。
	void update();
}
