import java.util.*;


// 監視対象のクラスのインターフェース
public abstract class ModelIF
{
	// 監視者への参照を格納するリスト(重複を避けるためSetを使用)
	Set<ObserverIF> observer_list = new HashSet<ObserverIF>();
	
	// 監視者リストに監視者を追加
	public void
	addObservers( ObserverIF observer)
	{
		observer_list.add( observer);
	}
	
	// 監視者をリストから外す
	public void
	deleteObserver( ObserverIF observer)
	{
		observer_list.remove( observer);
	}
	
	// 監視者に通知する
	protected void
	notifyObservers()
	{
		for( ObserverIF observer : observer_list) {
			// 各監視者のupdate()を呼び出す
			observer.update();
		}
	}
}
