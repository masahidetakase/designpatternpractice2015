// メイン

public class Main
{
	// 表示方式をGUIかまたはCUIで選択する。
	static final boolean Is_GUI_Mode = true;
	
	public static void
	main(String[] args)
	{
		// 恒温器のモデル Incubator のインスタンスを生成
		Incubator incubator = new Incubator( 15.f, 20.f, 45.f);

		// 温度が閾値を超えたら通知する先（Observer）をセット
		incubator.addObservers( new PopupAlert( incubator));
		incubator.addObservers( new BeepAlert());
		
		if( Is_GUI_Mode) {
			// イメージしやすいようにGUIをつけた。
			// このGUIのイベントループ中で、定期的にIncubatorのcheck()が呼ばれる。
			IncubatorGUI incubator_gui =
				IncubatorGUI.getInstance( incubator);
			
			incubator_gui.setVisible( true);
		} else {
			// 初期状態：サーモスタットON
			monitorIncubator( incubator, 8);
			
			// サーモスタットをOff
			incubator.setThermostatOn( false);
			
			monitorIncubator( incubator, 30);
			
			// サーモスタットをOn
			incubator.setThermostatOn( true);
			
			monitorIncubator( incubator, 30);
		}
	}
	
	// 恒温器の状態をモニタリングする（CUI用）。
	static void
	monitorIncubator( Incubator incubator, // 恒温器
					  int interval_sec) // モニタする時間
	{
		assert( incubator != null);
		assert( 0 < interval_sec);
		
		for( int i = 0; i < interval_sec; ++i) {
			String mesg =
				String.format( "Temperature: %03.1f deg.", incubator.getTemp());
				
			System.out.println( mesg);
			
			// 恒温器の状態チェック
			incubator.checkTemp();
			
			try {
				Thread.sleep( 1000);
			} catch( Exception e) {
				// 途中で割り込まれても気にしない。
			}
		}
	}
}
