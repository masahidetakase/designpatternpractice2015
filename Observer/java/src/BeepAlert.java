// BEEP音を鳴らして異常を知らせる
public class BeepAlert implements ObserverIF
{	
	// 観察対象を参照しないObserver
	public
	BeepAlert() {}
	
	@Override
	public void
	update()
	{
		java.awt.Toolkit.getDefaultToolkit().beep();
	}
}
