import java.awt.Dialog;
import java.awt.Window;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

// 警告メッセージを表示
public class PopupAlert extends JDialog implements ObserverIF
{
	private static final long serialVersionUID = 5306640098759855668L;
	
	private static final String Thermomete_Image_Name = "./thermometer.png";
	
	Incubator incubator;
	
	JLabel mesg_label = new JLabel( "");
	
	JLabel image_label = new JLabel( new ImageIcon( this.getClass().getResource( Thermomete_Image_Name)));

	public
	PopupAlert( ModelIF model)
	{
		super( (Window)null, "Alert!", Dialog.ModalityType.MODELESS);
		
		this.incubator = (Incubator)model;
		
		JPanel panel = new JPanel();
		
		panel.setLayout( new BoxLayout( panel, BoxLayout.X_AXIS));
		
		panel.add( mesg_label);
		
		panel.add( image_label);
		
		setContentPane( panel);
		
		setDefaultCloseOperation( JDialog.HIDE_ON_CLOSE);
	}

	@Override
	public void
	update()
	{
		String mesg = "ALERT! The temperature is " +
				String.format( "%03.1f", incubator.getTemp()) + " ℃";

		System.out.println( mesg);
		
		mesg_label.setText( mesg);
		
		pack();
		
		setVisible( true);
	}
}
