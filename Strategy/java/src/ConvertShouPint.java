// 升 <-> パイント（Pint）に変換する
public class ConvertShouPint extends UnitConverter
{
	static double Shou_Per_Pint = 0.31501696116202;
	
	public ConvertShouPint()
	{
		super( "升", "Pint");
	}
	
	// 升 -> パイント
	@Override
	public double
	fromLhs2Rhs(double lhs_val)
	{
		return lhs_val / Shou_Per_Pint;
	}

	// パイント -> 升
	@Override
	public double
	fromRhs2Lhs(double rhs_val)
	{
		return rhs_val * Shou_Per_Pint;
	}
}
