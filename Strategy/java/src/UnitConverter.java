// 単位変換を行うクラスの抽象クラス
abstract public class UnitConverter
{
	String unit_lhs; // 左側の数値の単位名
	
	String unit_rhs; // 右側の数値の単位の名前
	
	public
	UnitConverter( String unit_lhs,
					 String unit_rhs)
	{
		// preconditions
		assert( unit_lhs.equalsIgnoreCase( unit_rhs));
		
		this.unit_lhs = unit_lhs;
		this.unit_rhs = unit_rhs;
	}
	
	// 左側の単位名を取得
	public String
	leftUnit() { return unit_lhs;}
	
	// 右側の単位名を取得
	public String
	rightUnit() { return unit_rhs;}
	
	// 文字列化
	@Override
	public String toString()
	{
		return unit_lhs + " <-> " + unit_rhs;
	}

	// 左側->右側の単位変換を行う抽象メソッド
	// TODO: これをサブクラスで実装する。
	abstract public double
	fromLhs2Rhs( double lhs_val);
	
	// 右側->左側の単位変換を行う抽象メソッド
	// TODO: これをサブクラスで実装する。
	abstract public double
	fromRhs2Lhs( double rhs_val);
}
