// ラジアン <-> 度（degree）変換
public class ConvertRadianDegree extends UnitConverter
{
	private static double Radian_Per_Degree = Math.PI / 180.; 
	
	public
	ConvertRadianDegree()
	{
		super( "Rad.", "Deg.");
	}

	// Radian -> Degree
	@Override
	public double
	fromLhs2Rhs(double lhs_val)
	{
		return lhs_val / Radian_Per_Degree;
	}

	// Degree -> Radian
	@Override
	public double
	fromRhs2Lhs(double rhs_val)
	{
		return rhs_val * Radian_Per_Degree;
	}

}

