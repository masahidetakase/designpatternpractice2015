/*
 * メインウィンドウを司どるクラス。
 * 以下の主なコントロールを有する。
 *   変換する単位を選択するコンボボックス
 *   左辺、右辺値を入力するテキストボックス（編集可）
 *   左辺->右辺 変換ボタン
 *   右辺->左辺 変換ボタン
 */

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import java.awt.event.*;


// メインウィンドウ
public class MainFrame extends JFrame
{
	private static final long serialVersionUID = 1L;
	
	JPanel panel = new JPanel(); // これにコントロールを配置
	
	int control_height;
	
	JComboBox<UnitConverter> converter_chooser;

	JTextField rhs_val_field = new JTextField( 10); // 左辺値
	
	JLabel left_unit_label = new JLabel(""); // 左の単位のラベル
	
	JTextField lhs_val_field = new JTextField( 10); // 右辺値
	
	JLabel right_unit_label = new JLabel(""); // 右の単位のラベル
	
	// 左辺->右辺 変換ボタン
	JButton lhs_to_rhs_btn = new JButton( "Left to Right");
	
	// 右辺->左辺 変換ボタン
	JButton rhs_to_lhs_btn = new  JButton( "Right to Left");
	
	// 単位変換オブジェクトの配列。ここに単位変換クラスを追加する。
	// MVC とか堅いことは言わないで。
	// TODO: ここに単位変換クラスを追加してみましょう。
	UnitConverter converters[] = {
		new ConvertGallonLiter(),
		new ConvertShouPint(),
		new ConvertMileKm(),
		new ConvertPsWatt(),
		new ConvertRadianDegree()
		};
	
	public
	MainFrame()
	{
		super( "Unit converter");	
		
		setSize( 400, 200);
		
		panel = new JPanel();
		
		// コントロールを縦に積む
		panel.setLayout( new BoxLayout( panel, BoxLayout.Y_AXIS));
		
		panel.add( createUnitConverterChooser());
		panel.add( createValueFields());
		panel.add( createConvertButtons());
		
		// コントロールを載せたパネルをメインフレームに追加
		add( panel);
		
		pack();
		
		setDefaultCloseOperation( EXIT_ON_CLOSE);
	}
	
	// 単位を選択するコンボボックスを初期化
	JPanel
	createUnitConverterChooser()
	{
		JPanel vpanel = new JPanel();
		vpanel.setBorder( new EmptyBorder( 10, 10, 10, 10));			
		
		vpanel.add( new JLabel( "Unit Converter:"));
		vpanel.add( Box.createRigidArea(new Dimension(5,5)));
		converter_chooser = new JComboBox<UnitConverter>( converters);
		converter_chooser.setEditable( false);
		
		// イベントリスナの設定
		converter_chooser.addItemListener(
				new ItemListener() {
					
					@Override
					public void itemStateChanged(ItemEvent e) {
						// 単位が変更されたら
						if( e.getStateChange() == ItemEvent.SELECTED) {
							UnitConverter cur_conv =
									(UnitConverter)e.getItem();
							
							// 単位名を更新
							left_unit_label.setText( cur_conv.leftUnit());
							right_unit_label.setText( cur_conv.rightUnit());
						}
					}
				});
		
		vpanel.add( converter_chooser);
		
		return vpanel;
	}
	
	// 数値を入力するテキストボックスを初期化
	JPanel
	createValueFields()
	{
		JPanel vpanel = new JPanel();
		vpanel.setBorder( new EmptyBorder( 10, 10, 10, 10));
		vpanel.setLayout( new BoxLayout( vpanel, BoxLayout.X_AXIS));
		
		double lhs_val = 1.;
		double rhs_val = converters[0].fromLhs2Rhs( lhs_val);
		
		vpanel.add( Box.createRigidArea(new Dimension(10,5)));
		
		lhs_val_field.setText( Double.toString( lhs_val));
		vpanel.add( lhs_val_field);
		
		vpanel.add( Box.createRigidArea(new Dimension(5,5)));
		
		left_unit_label.setText( converters[0].leftUnit());
		vpanel.add( left_unit_label);
		
		vpanel.add( new JLabel( " = "));
		
		rhs_val_field.setText( dblToString( rhs_val));
		vpanel.add( rhs_val_field);
		
		vpanel.add( Box.createRigidArea(new Dimension(5,5)));
		
		right_unit_label.setText( converters[0].rightUnit());
		vpanel.add( right_unit_label);
		
		vpanel.add( Box.createRigidArea(new Dimension(5,5)));
		
		return vpanel;
	}
	
	// 変換方向を指定するボタン
	JPanel
	createConvertButtons()
	{
		JPanel vpanel = new JPanel();
		vpanel.setBorder( new EmptyBorder( 10, 10, 10, 10));
		
		vpanel.setLayout( new BoxLayout( vpanel, BoxLayout.X_AXIS));
		vpanel.add( lhs_to_rhs_btn);
		lhs_to_rhs_btn.addActionListener( new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				UnitConverter cur_conv =
						(UnitConverter)converter_chooser.getSelectedItem();
				
				double lhs_val = Double.parseDouble( lhs_val_field.getText());
				double rhs_val = cur_conv.fromLhs2Rhs( lhs_val);
				
				rhs_val_field.setText( dblToString( rhs_val));
			}
		});
		
		vpanel.add( Box.createRigidArea(new Dimension(20,10)));
		vpanel.add( rhs_to_lhs_btn);
		rhs_to_lhs_btn.addActionListener( new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				UnitConverter cur_conv =
						(UnitConverter)converter_chooser.getSelectedItem();
				
				double rhs_val = Double.parseDouble( rhs_val_field.getText());
				double lhs_val = cur_conv.fromRhs2Lhs( rhs_val);
				
				lhs_val_field.setText( dblToString( lhs_val));
			}
		});
		
		return vpanel;
	}
	
	String
	dblToString( double val)
	{
		if( (-10. < val) && (val < 10.)) {
			return String.format( "%.5f", val);
		} else {
			return String.format( "%.4f", val);
		}
	}
}
