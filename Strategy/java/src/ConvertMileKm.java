// マイル<->キロメートル 間変換クラス
public class ConvertMileKm extends UnitConverter
{
	private static double Mile_Per_Km = 1.609344;
	
	public
	ConvertMileKm()
	{
		super( "Mile", "Km");
	}

	// Mile -> Km
	@Override
	public double
	fromLhs2Rhs(double lhs_val)
	{
		return lhs_val * Mile_Per_Km;
	}

	// Km -< Mile
	@Override
	public double
	fromRhs2Lhs(double rhs_val)
	{
		return rhs_val / Mile_Per_Km;
	}

}
