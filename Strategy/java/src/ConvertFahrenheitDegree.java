// 華氏 <-> 摂氏 間変換クラス
// F = 9/5C + 32
// C = 5/9(F-32)
// TODO:このクラスを完成させ、変換オブジェクトをコンボボックスに追加しましょう。
public class ConvertFahrenheitDegree extends UnitConverter
{
	public
	ConvertFahrenheitDegree()
	{
		super( "°F", "°C");
	}
	
	// 華氏 -> 摂氏
	@Override
	public double fromLhs2Rhs(double lhs_val)
	{
		// TODO: 実装しましょう。
		return 0;
	}

	// 摂氏 -> 華氏
	@Override
	public double fromRhs2Lhs(double rhs_val)
	{
		// TODO: 実装しましょう。
		return 0;
	}
}
