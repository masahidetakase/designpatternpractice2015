// 英馬力(PS) <-> キロワット 間変換
public class ConvertPsWatt extends UnitConverter
{
	private static double Ps_Per_Watt = 745.699871;
	
	public
	ConvertPsWatt()
	{
		super( "PS", "W");
	}

	// PS -> W
	@Override
	public double
	fromLhs2Rhs(double lhs_val)
	{
		return lhs_val * Ps_Per_Watt;
	}

	// W -> PS
	@Override
	public double
	fromRhs2Lhs(double rhs_val)
	{
		return rhs_val / Ps_Per_Watt;
	}
}
