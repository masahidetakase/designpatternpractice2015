// ガロン<->リットル 間変換クラス
public class ConvertGallonLiter extends UnitConverter {

	private static double Liter_Per_Gallon = 3.78541178;
	
	public
	ConvertGallonLiter()
	{
		super( "Gallon", "Liter");
	}
	
	// Gallon -> Liter
	@Override
	public double fromLhs2Rhs( double lhs_val)
	{
		return lhs_val * Liter_Per_Gallon;
	}

	// Liter -> Gallon
	@Override
	public double fromRhs2Lhs(double rhs_val)
	{
		return rhs_val /  Liter_Per_Gallon;
	}
}
