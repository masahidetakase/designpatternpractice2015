import java.util.Iterator;

// コントローラークラス
public class Controller
{
	CarDataBase cardb;
	
	// TODO: ここに燃費で検索するStrategyを追加しましょう。
	CriterionFactory[] Criterion_Factories =
		{
			new DummyCriterionFactory(),
			new ManufacCriterionFactory(),
			new FuelTypeCriterionFactory(),
			new EngineCapacityCriterionFactory()
		};
	
	public
	Controller( CarDataBase cardb)
	{
		this.cardb = cardb;
	}
	
	// 検索条件のファクトリの配列を取得する
	public CriterionFactory[]
	getCriterionFactories()
	{
		return Criterion_Factories;
	}
	
	// 検索条件を受け取り、そのイテレータを返す
	public Iterator<CarInfo>
	getIterator( Criterion criterion)
	{
		return cardb.iterator( criterion);
	}
}
