// 何もしない検索のStrategy：DummyCriterionのFactory
public class DummyCriterionFactory extends CriterionFactory
{
	public DummyCriterionFactory()
	{
		super( " --- ", 0);
	}
	
	// ファクトリメソッド
	@Override
	public Criterion
	create(String arg1, String arg2)
	{
		return new DummyCriterion( arg1, arg2);
	}
}

// 何もしない検索のStrategy。常に真を返す。
class DummyCriterion extends Criterion
{
	public
	DummyCriterion( String arg1, String arg2)
	{
		super(); // パラメタを取らない
	}
		
	@Override
	protected boolean
	detect(CarInfo car_info)
	{
		return true; // 無条件に真を返す
	}
}
