import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Iterator;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

class CriterionPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	// 検索基準を
	JComboBox<CriterionFactory> criterion_chooser;
	
	JTextField param1_filed = new JTextField( 20); // 検索パラメタ１
	
	JTextField param2_filed = new JTextField( 20); // 検索パラメタ2
	
	public 
	CriterionPanel( CriterionFactory[] criterion_factories)
	{
		super();
		
		this.criterion_chooser =
				new JComboBox<CriterionFactory>( criterion_factories);
		
		criterion_chooser.setEditable( false);
		
		setLayout( new BoxLayout( this, BoxLayout.X_AXIS));
		
		setBorder( new EmptyBorder( 5, 5, 5, 5));
		
		add( criterion_chooser);
		add( Box.createRigidArea(new Dimension(5,1)));
		add( param1_filed);
		add( param2_filed);
		add(Box.createRigidArea(new Dimension(5,1)));
		
		setActions();
	}
	
	// 検索条件に応じて入力フィールドが有効/無効化されるように設定
	void setActions()
	{
		criterion_chooser.addItemListener(
			new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					// 検索条件のファクトリが要求するパラメタ数に応じて、フィールド数を調整する
					if( e.getStateChange() == ItemEvent.SELECTED) {
						CriterionFactory criterion_factory = (CriterionFactory)e.getItem();
						suitFields( criterion_factory);
					}
				}
			});
		
		criterion_chooser.setSelectedIndex( 0);
		suitFields( (CriterionFactory)criterion_chooser.getSelectedItem());
	}
	
	// 検索条件のファクトリが要求するパラメタ数に応じて、フィールド数を調整する
	void
	suitFields( CriterionFactory criterion_factory)
	{
		switch( criterion_factory.getNumArgs()) {
			case 2:
				param1_filed.setEnabled( true);
				param2_filed.setEnabled( true);
				param1_filed.setBackground( Color.WHITE);
				param2_filed.setBackground( Color.WHITE);
				break;
			case 1:
				param1_filed.setEnabled( true);
				param2_filed.setEnabled( false);
				param1_filed.setBackground( Color.WHITE);
				param2_filed.setBackground( Color.GRAY);
				break;
			case 0:
				param1_filed.setEnabled( false);
				param2_filed.setEnabled( false);
				param1_filed.setBackground( Color.GRAY);
				param2_filed.setBackground( Color.GRAY);
				break;
			default:
				assert( false);
				break;
		}
	}
	
	// 選択されている検索条件を取得
	public Criterion
	getCurrentCriterion()
	{
		CriterionFactory criterion_factory =
			(CriterionFactory)criterion_chooser.getSelectedItem();
		
		Criterion criterion =
			criterion_factory.create( param1_filed.getText().trim(),
										 param2_filed.getText().trim());
		
		return criterion;
	}
}

// メインウィンドウのクラス
public class MainFrame extends JFrame
{
	private static final long serialVersionUID = 167530063407917403L;
	
	Controller controller;
	
	JPanel panel = new JPanel();
	
	// 検索条件のパネル
	// TODO: 指定できる検索条件の数を増やしてみましょう。
	CriterionPanel[] criterion_panels = new CriterionPanel[3];
			
	public
	MainFrame( Controller controller)
	{
		super( "Car search App.");
		
		this.controller = controller;
		
		panel.setLayout( new BoxLayout( panel, BoxLayout.Y_AXIS));
		
		// 検索条件入力パネルを追加
		for( int idx =0; idx < criterion_panels.length; ++idx) {
			criterion_panels[idx] =
				new CriterionPanel( controller.getCriterionFactories());
			
			panel.add( criterion_panels[idx]);
		}
		
		panel.add( Box.createRigidArea(new Dimension(5,5)));
		
		panel.add( createSearchButton());
				
		add( panel);
		
		pack();
		
		setDefaultCloseOperation( EXIT_ON_CLOSE);
	}
	
	// 検索ボタンを作成する
	JButton
	createSearchButton()
	{
		JButton search_btn = new JButton( "Search");
		
		// 検索ボタンをされた場合の動作を定義
		search_btn.addActionListener(
			new ActionListener() {
				@Override
				public void
				actionPerformed(ActionEvent e)
				{
					showSearchResults();
				}
			});
		
		return search_btn;
	}
	
	// 連結した検索条件を取得する。
	Criterion
	getCriteria()
	{
		// 最初の検索条件を取得
		Criterion first = criterion_panels[0].getCurrentCriterion();
		
		Criterion tail = first;
				
		// 残りの検索条件を数珠つなぎにつなぐ
		for( int idx = 1; idx < criterion_panels.length; ++idx) {
			tail = tail.add( criterion_panels[idx].getCurrentCriterion());
		}
		
		return first;
	}
	
	// 検索結果ダイアログを表示する
	void
	showSearchResults()
	{
		// 複合条件を生成する
		Criterion criteria = getCriteria();
		
		JDialog dialog =
			new SearchResultDialog( this, controller.getIterator( criteria));
		
		dialog.setVisible(true);
		
	}
}

// 検索結果を表示するダイアログ
class SearchResultDialog extends JDialog
{	
	private static final long serialVersionUID = 1L;

	Iterator<CarInfo> iter;
	
	DefaultTableModel table_model; // "テーブル"のモデル
	
	// カラム名
	static String col_names[] = {
			"Manufac.",
			"Model",
			"Year",
			"Description",
			"Fuel type",
			"Transmission",
			"Engine capacity(cm3)",
			"Combined metrics(L/100km)"};
	
	public
	SearchResultDialog( JFrame parent, Iterator<CarInfo> iter)
	{
		super( parent, true);
		
		this.iter = iter;
		
		JPanel panel = new JPanel();
		panel.setLayout( new BoxLayout( panel, BoxLayout.Y_AXIS));
		
		// ”閉じる"ボタンをつける
		panel.add( createCloseButton());
		
		// "テーブル"のモデル
		table_model = new DefaultTableModel(col_names, 0);
		
		// 下の"table_model"のビューに相当する
		JTable table = new JTable(table_model);

		// データをモデルに追加
		insertSearchResults();
		
		panel.add( new JScrollPane( table));
		
		add( panel);
					
		pack();
	}
	
	// ウインドウを閉じる。
	void
	close() { this.setVisible( false);}
	
	// "閉じる"ボタンを作成する
	JButton
	createCloseButton()
	{
		JButton close_btn = new JButton("Close");
		
		close_btn.addActionListener(
			new ActionListener() {
				@Override
				public void
				actionPerformed(ActionEvent e)
				{
					close();
				}
			});
		
		return close_btn;
	}
	
	void
	insertSearchResults()
	{
		// イテレータを介してデータを取得し、テーブルに追加
		while( iter.hasNext()) {
			CarInfo info = iter.next();
			System.out.println( "  " + info.toString());
			
			String[] row = {
				info.getManufacture().trim(),
				info.getModel().trim(),
				Integer.toString( info.getYear()),
				info.getDescription().trim(),
				info.getFuelType().toString(),
				info.getTransmissionType().toString(),
				Float.toString( info.getEngineCapacity()),
				Float.toString( info.getMetric())
			};
			
			table_model.addRow( row); // 行を追加
		}
	}
}
