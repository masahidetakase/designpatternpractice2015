// 排気量で検索するためのStrategy:EngineCapacityCriterionFactoryのファクトリ
public class EngineCapacityCriterionFactory extends CriterionFactory
{
	public
	EngineCapacityCriterionFactory()
	{
		super( "Engine capacity(cm3)", 2);
	}
	
	// ファクトリメソッド
	@Override
	public Criterion
	create( String arg1, String arg2)
	{
		try {
			float arg1_val = Float.parseFloat( arg1);
			float arg2_val = Float.parseFloat( arg2);
			
			return new EngineCapacityCriterion( arg1_val, arg2_val);
		} catch( NumberFormatException e) {
			return new EngineCapacityCriterion( 0, Float.MAX_VALUE);
		}
	}
}


// 排気量で検索するためのStrategy
class EngineCapacityCriterion extends Criterion
{	
	float lower_limit = 0.f; // 排気量の下限
	
	float upper_limit = Float.MAX_VALUE ; // 排気量の上限
	
	public
	EngineCapacityCriterion( float arg1, float arg2)
	{
		super();
		
		if( arg1 < arg2) {
			this.lower_limit = arg1;
			this.upper_limit = arg2; 
		} else {
			this.lower_limit = arg2;
			this.upper_limit = arg1;
		}
	}

	// 排気量が指定された範囲なら真を返す。
	@Override
	protected boolean
	detect(CarInfo car_info)
	{
		float capacity = car_info.getEngineCapacity();
		
		return ( lower_limit <= capacity) && ( capacity <= upper_limit);
	}
}
