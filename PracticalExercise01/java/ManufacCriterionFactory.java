// メーカーの名前で検索するためのStrategyのファクトリ
class ManufacCriterionFactory extends CriterionFactory
{
	public
	ManufacCriterionFactory()
	{
		super( "Manufacture", 1);
	}
	
	@Override
	public Criterion create(String arg1, String arg2)
	{	
		return new ManufacCriterion( arg1);
	}
}

// メーカーの名前で検索するためのStrategy
class ManufacCriterion extends Criterion
{
	String key_str = null; // 検索用文字列
	
	public
	ManufacCriterion( String key_str)
	{
		super();
		this.key_str = key_str.toLowerCase();
	}

	@Override
	public boolean
	detect(CarInfo car_info)
	{
		assert( key_str != null);
		
		// メーカーの文字列を小文字化
		String manufac_str = 
			car_info.getManufacture().toString().toLowerCase();
		
		return 0 <= manufac_str.indexOf( key_str);
	}
}
