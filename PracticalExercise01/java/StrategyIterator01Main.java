import javax.swing.JFrame;

public class StrategyIterator01Main
{
	public static void main(String[] args)
	{
		try {
			CarDataBase cardb = new CarDataBase( args[0]); // モデル
			
			Controller controller = new Controller( cardb); // コントローラ
			
			JFrame main_frame = new MainFrame( controller); // ビュー
			
			main_frame.setVisible( true);
		
		} catch( Exception e) {
			System.err.println( "App. initializtaion failed.");
		}
	}
}
