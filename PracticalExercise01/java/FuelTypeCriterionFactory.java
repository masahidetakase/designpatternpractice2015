// 燃料の種類で検索するためのStrategyのファクトリ
public class FuelTypeCriterionFactory extends CriterionFactory
{
	public
	FuelTypeCriterionFactory()
	{
		super( "Fuel type", 1);
	}
	
	// ファクトリメソッド
	@Override
	public Criterion create(String arg1, String arg2)
	{
		return new FuelTypeCriterion( arg1);
	}
}

// 燃料の種類で検索するためのStrategyクラス
class FuelTypeCriterion extends Criterion
{
	String key_str = null;
	
	public
	FuelTypeCriterion( String key_str)
	{
		super();
		this.key_str = key_str.toLowerCase();
	}

	@Override
	protected boolean
	detect(CarInfo car_info)
	{
		assert( key_str != null);
		
		// 燃料の文字列を小文字化
		String fuel_str = 
			car_info.getFuelType().toString().toLowerCase();
		
		return fuel_str.startsWith( key_str);
	}
}
