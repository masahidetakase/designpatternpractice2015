// 車種情報の検索条件のインターフェース
// パラメタを最大2つ持つ
abstract public class Criterion
{	
	Criterion next_criterion = null; // リンクされた検索条件
	
	public
	Criterion() {}
	
	// 検索条件を連結する。
	public Criterion
	add( Criterion criterion)
	{
		// 循環参照を禁止
		if( this == criterion) { 
			throw new IllegalArgumentException( "Circular reference.");
		}
		
		this.next_criterion = criterion;
		
		return next_criterion;
	}
	
	// 判定を行う。もし、リンクされた検索条件があれば、それを実行する。
	public boolean
	execute( CarInfo car_info)
	{
		if( detect( car_info)) { // 条件に合致したら
			// 次の条件があれば、それを実行。
			if( next_criterion != null) {
				return next_criterion.execute( car_info);
			} else {
				return true; // 次がなければ真を返す。
			}
		}
		
		return false;
	}
	
	// 検索条件のTemplate method。CarInfoが条件に合致するか判定する。
	abstract protected boolean
	detect( CarInfo car_info);
}
