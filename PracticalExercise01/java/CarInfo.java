// 車種情報を格納するエンティティクラス
public class CarInfo implements Cloneable
{
	// MT/ATの識別するための列挙型
	enum TransmissionType
	{
		Manual( "MT"),
		Auto( "AT"),
		SemiAuto( "SAT"),
		Unknown( "unknown");
		
		private String name;
		
		TransmissionType( String name) { this.name = name;}
		
		/* 文字列化するためのメソッド。最も基底の型ObjectのtoString()メソッド
		 * をオーバーロード。
		 */
		@Override
		public String toString() { return name;}
		
		/* 文字列から適切なEnumを生成する。*/
		public static TransmissionType
		getType( String trans_str)
		{
			// Java7 or later
			switch( trans_str.trim()) {
				case "Manual":
					return Manual;
				case "Automatic":
					return Auto;
				case "SAT":
					return SemiAuto;
				default:
					return Unknown;
			}
		}
	}
	
	// 燃料の種類を示す列挙型
	enum FuelType
	{
		Petrol( "Petrol"),
		Diesel( "Diesel"),
		LPG( "LPG"),
		Petrol_Hybrid( "Petrol Hybrid"),
		CNG( "CNG"),
		Petrol_Electric( "Petrol Electric"),
		LPG_Petrol( "LPG / Petrol"),
		Petrol_E85( "Petrol / E85 (Flex Fuel)"),
		Diesel_Electric( "Diesel Electric"),
		Electricity_Petrol( "Electricity/Petrol"),
		Electricity_Diesel( "Electricity/Diesel"),
		Unknown( "Unknown");
		
		private String name;
		
		FuelType( String name) { this.name = name;}
		
		@Override
		public String toString() { return name;}
		
		/* 文字列から適切なEnumを生成する。*/
		public static FuelType
		getType( String fuel_str)
		{
			// Java7 or later
			switch( fuel_str.trim()) {
				case "Petrol":
					return Petrol;
				case "Diesel":
					return Diesel;
				case "LPG":
					return LPG;
				case "Petrol Hybrid":
					return Petrol_Hybrid;
				case "CNG":
					return CNG;
				case "Petrol Electric":
					return Petrol_Electric;
				case "LPG / Petrol":
					return LPG_Petrol;
				case "Petrol / E85 (Flex Fuel)":
					return Petrol_E85;
				case "Diesel Electric":
					return Diesel_Electric;
				case "Electricity/Petrol":
					return Electricity_Diesel;
				case "Electricity/Diesel":
					return Electricity_Diesel;
				default:
					return Unknown;
			}
		}
	}
	
	private String manufacture;
	
	private String model;
	
	private int year;
	
	private String description;
	
	private TransmissionType transmission;
	
	private FuelType fuel_type; 
	
	private float engine_capacity;
	
	private float metric;
	
	public
	CarInfo( String manufacture,
			  String model,
			  int year,
			  FuelType fuel,
			  TransmissionType transmission,
			  float engine_capacity,
			  float metric,
			  String description)
	{
		this.manufacture = manufacture;
		this.model = model;
		this.year = year;
		this.fuel_type = fuel;
		this.transmission = transmission;
		this.engine_capacity = engine_capacity;
		this.metric = metric;
		this.description = description;
	}
	
	public CarInfo
	clone() throws CloneNotSupportedException 
	{
		return new CarInfo( manufacture,
							model,
				            year,
				            fuel_type,
				            transmission,
				            engine_capacity,
				            metric,
				            description);
	}
	
	// 文字列化
	@Override
	public String
	toString()
	{
		StringBuilder stringize = new StringBuilder();
		
		stringize.append( manufacture);
		
		stringize.append( " ");
		
		stringize.append( year);
		
		stringize.append( " Model:\"");
		stringize.append( model);	
		stringize.append( "\" ");
		
		stringize.append( "Transm:");
		stringize.append( transmission);
		
		stringize.append( " Fuel:");
		stringize.append( fuel_type);
				
		return new String( stringize);
	}
	
	public String
	getManufacture() { return manufacture;}
	
	public String
	getModel() { return model;}
	
	public int
	getYear() { return year;}
	
	public FuelType
	getFuelType() { return fuel_type;}
	
	public TransmissionType
	getTransmissionType() { return transmission;}
	
	public float
	getEngineCapacity() { return engine_capacity;}
	
	public float
	getMetric() { return metric;}
	
	public String
	getDescription() { return description;}
	
}
