// 検索条件のファクトリの抽象クラス
abstract public class CriterionFactory
{
	String name;
	
	int num_args;
	
	protected
	CriterionFactory( String name, int num_args)
	{
		this.name = name;
		this.num_args = num_args;
	}
	
	abstract public Criterion
	create( String arg1, String arg2);
	
	@Override
	public String
	toString() { return name;}
	
	public int
	getNumArgs() { return num_args;}
}
