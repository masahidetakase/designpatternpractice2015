import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import com.opencsv.CSVReader;

// 車種情報を格納する
public class CarDataBase implements Iterable<CarInfo>
{
	private List<CarInfo> car_list;

	public CarDataBase( String cardb_csv_path) throws Exception
	{
		car_list = new ArrayList<CarInfo>();

		try{
			// OpenCSV を使ってCSVファイルを読み込む。
			CSVReader csv_reader = new CSVReader( new FileReader( cardb_csv_path), ',', '"', 1);

			// CSVファイルから車種情報を読み込む。
			// 1ラインはカラムを要素とする文字列配列になる。
			String[] line;
			while( (line = csv_reader.readNext()) != null) {
				CarInfo car_info = new CarInfo( line[1],
												line[2],
												Integer.parseInt( line[0]),
												CarInfo.FuelType.getType( line[6]),
												CarInfo.TransmissionType.getType( line[4]),
												Float.parseFloat( line[5]),
												Float.parseFloat( line[7]),
												line[3]);
				// コレクションに追加
				car_list.add( car_info);
			}

			csv_reader.close();

		} catch( IOException ex) {
			throw new Exception( "Reading car database failed. Sorry.");
		}
	}

	// Iterableの実装。順方向イテレータを提供
	public Iterator<CarInfo>
	iterator()
	{
		return car_list.iterator();
	}

	// 指定された検索条件に合致した車種情報を指すイテレータを返す。
	public Iterator<CarInfo>
	iterator( Criterion criteria)
	{
		return new CriteriaIterator( car_list.listIterator(), criteria);
	}
}

//検索条件に合致する車種情報を指すイテレータ
class CriteriaIterator implements Iterator<CarInfo>
{
	// 双方向移動可能なイテレータ
	ListIterator<CarInfo> car_list_iter;
	
	Criterion criteria;
	
	public
	CriteriaIterator( ListIterator<CarInfo> car_list_iter,
						Criterion criteria)
	{
		this.car_list_iter = car_list_iter;
		this.criteria = criteria;
	}
	
	public boolean hasNext() {
		while( car_list_iter.hasNext()) {
			// メーカー名と検索文字列が部分一致すれば、
			if( criteria.execute( car_list_iter.next())) {
				car_list_iter.previous(); // イテレータを一つ戻す
				return true;
			}
		}
		
		return false;
	}

	@Override
	public CarInfo next() {
		return car_list_iter.next();
	}
	
}