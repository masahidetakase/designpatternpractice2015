// Iterator パターンのサンプル
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;



public class IteratorMain {

	/* Iteratorが指す要素を表示する。
	 * コレクションの型によらず、共通のインターフェースIteratorで
	 * その要素にアクセスできることに注目。
	 */
	public static void showElements( Iterator<String> iter)
	{
		while( iter.hasNext()) {
			// Iteratorのnext() メソッドは、現在の要素への参照を返して次に進む。
			System.out.println( "    要素：" + iter.next());
		}
		
		System.out.println( "----------" + System.lineSeparator());
	}
	
	public static void main(String[] args) {
		
		// 配列を生成・初期化する
		List<String> prefectureArray =
				new ArrayList<String>() {{
					add( "北海道"); 
					add( "青森");
					add( "秋田");
					add( "岩手");
					add( "山形");
					add( "宮城");
					add( "新潟");
					add( "福島");}};
		
		// リンクドリストを生成
		List<String> prefectureLinkedList =
				new LinkedList<String>( prefectureArray);
		
		// ハッシュセットを生成
		// TODO: HashSetを使ってみましょう。
		
		// コレクションの要素を指すイテレータ
		// Javaのイテレータは要素の間を指す。
		Iterator<String> iterator = null;
		
		System.out.println( "ArrayListの中身を表示");
		// iteratorはArrayListのイテレータ
		iterator = prefectureArray.iterator();
		showElements( iterator);
		
		System.out.println( "LinkedListの中身を表示");
		// iteratorはLinkedListのイテレータ
		iterator = prefectureLinkedList.iterator();
		showElements( iterator);
		
		System.out.println( "HashSetの中身を表示");
		// iteratorはHashSetのイテレータ
		// TODO：ここでHashSetのイテレータを生成します。
		showElements( iterator);	
		
		// これも内部でイテレータを使っている
		System.out.println( "拡張forでコレクションを走査");
		for( String prefecture : prefectureArray)
		{
			System.out.println( "    要素：" + prefecture);
		}
	}

}
