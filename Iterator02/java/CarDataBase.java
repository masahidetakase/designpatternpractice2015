import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.opencsv.CSVReader;

// メーカー名が検索文字列と一致するものを取得する順方向イテレータ。
class ManufacIterator implements Iterator<CarInfo>
{
	private String key_substr; // 検索文字列

	// 双方向移動可能なイテレータ
	private ListIterator<CarInfo> car_list_iter;

	public
	ManufacIterator( ListIterator<CarInfo> car_list_iter,
					   String key_substr)
	{
		this.car_list_iter = car_list_iter;
		this.key_substr = key_substr.toLowerCase(); // 検索文字列は小文字に統一
	}

	public boolean
	hasNext()
	{
		while( car_list_iter.hasNext()) {
			// イテレータの指す値を取得し、次へ進める。
			String manufac = car_list_iter.next().getManufacture().toLowerCase();

			// メーカー名と検索文字列が部分一致すれば、
			if( manufac.indexOf( key_substr) != -1) {
				car_list_iter.previous(); // イテレータを一つ戻す
				return true;
			}
		}

		return false;
	}

	public CarInfo
	next()
	{
		return car_list_iter.next();
	}
}

// 燃料で検索するイテレータ
class FuelIterator implements Iterator<CarInfo>
{
	// 双方向移動可能なイテレータ
	private ListIterator<CarInfo> car_list_iter;

	CarInfo.FuelType fuel_type;

	public
	FuelIterator( ListIterator<CarInfo> car_list_iter,
					CarInfo.FuelType fuel_type)
	{
		this.car_list_iter = car_list_iter;
		this.fuel_type = fuel_type;
	}

	public boolean
	hasNext()
	{
		// TODO: 実装してみましょう。
		return false;
	}

	public CarInfo
	next()
	{
		return car_list_iter.next();
	}
}


// 車種情報を格納する
public class CarDataBase implements Iterable<CarInfo>
{

	private List<CarInfo> car_list;

	public CarDataBase( String cardb_csv_path) throws Exception
	{
		car_list = new ArrayList<CarInfo>();

		try{
			// OpenCSV を使ってCSVファイルを読み込む。
			CSVReader csv_reader = new CSVReader( new FileReader( cardb_csv_path), ',', '"', 1);

			// CSVファイルから車種情報を読み込む。
			// 1ラインはカラムを要素とする文字列配列になる。
			String[] line;
			while( (line = csv_reader.readNext()) != null) {
				CarInfo car_info = new CarInfo( line[1],
												line[2],
												Integer.parseInt( line[0]),
												CarInfo.FuelType.getType( line[6]),
												CarInfo.TransmissionType.getType( line[4]),
												Float.parseFloat( line[5]),
												Float.parseFloat( line[7]),
												line[3]);
				// コレクションに追加
				car_list.add( car_info);
			}

			csv_reader.close();

		} catch( IOException ex) {
			throw new Exception( "Reading car database failed. Sorry.");
		}
	}

	// Iterableの実装。順方向イテレータを提供
	public Iterator<CarInfo>
	iterator()
	{
		return car_list.iterator();
	}

	// メーカー名が検索文字列と一致するものを探すイテレータを取得する
	public Iterator<CarInfo>
	IteratorByManufac( String key_substr)
	{
		return new ManufacIterator( car_list.listIterator(),
									key_substr);
	}

	// 燃料の種類が一致するものを探すイテレータを取得する
	public Iterator<CarInfo>
	IteratorByFuel( CarInfo.FuelType fuel_type)
	{
		// TODO 実装してみてください。
		return null;
	}
}
