import java.util.Iterator;



public class Iterator2Main
{
	public static void main(String[] args) {
		try {
			// CSV ファイルから車種情報を読み込む
			CarDataBase cardb = new CarDataBase( args[0]);

			// メーカーの名前で検索するイテレータ
			Iterator<CarInfo> iter = cardb.IteratorByManufac( "Merce");

			// TODO ： 燃料の種類で検索するイテレータを作ってみましょう。

			while( iter.hasNext()) {
				System.out.println( iter.next());
			}
		} catch( Exception ex) {
			ex.printStackTrace();
		}
	}

}
