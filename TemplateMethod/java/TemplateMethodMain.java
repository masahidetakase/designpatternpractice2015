// 入力データを蓄積し、その平均を求める親クラス
abstract class AbstractMeanCalc
{
	protected int num; // データ数
	
	protected double amount; // 入力されたデータの合計
	
	public AbstractMeanCalc()
	{
		this.num = 0;
		this.amount = 0.;
	}
	
	// データを追加する
	void addData( double val)
	{
		// 異常な値でなければ加算
		if( validate( val)) {
			amount += val;
			++num;
		} else {
			System.out.println( "  Invalid value:" + val);
		}
	}
	
	// 平均を計算する。
	double calc() {
		if( num == 0) { return 0.;}
		
		return amount / num;
	}
	
	// 入力値を検査し、異常値であれば偽を返す。
	abstract boolean validate( double val);
}


//入力データのチェックをしない子クラス
class SimpleMeanCalc extends AbstractMeanCalc
{
	// 抽象メソッドをオーバーライド
	@Override
	boolean validate( double val)
	{
		// チェックしない
		return true;
	}
}

// 入力データのチェックする子クラス
class StrictMeanCalc extends AbstractMeanCalc
{
	// データの二乗和
	double squared_amount;
	
	public StrictMeanCalc() {
		super();
		squared_amount = 0.;
	}
	
	// 抽象メソッドをオーバーライド
	@Override
	boolean validate( double val)
	{
		if( num < 8) {
			squared_amount += Math.pow( val, 2);
			return true;
		}
		
		// 平均
		double mean = amount / num;
		
		// 分散
		double vr = squared_amount / num - Math.pow( mean, 2);
		
		// 標準偏差
		double sd = Math.sqrt( vr);
		
		// 平均値とのずれが標準偏差の2倍以上なら異常値とみなす。
		if( Math.abs( mean - val) > sd * 2) {
			return false;
		}
		
		// 二乗和を更新
		squared_amount += Math.pow( val, 2);
			
		return true;
	}
}

public class TemplateMethodMain {
	
	public static void main(String[] args) {
		
		// データ
		double data[] = { 0.3, 0.5, 0.8, 0.55, 0.9, 0.34, 0.67, 0.92, 1.85, 3.23, 0.59, -0.55};
		
		// TODO: 片方をコメントアウトして、動作を確認
		//AbstractMeanCalc meanCalc = new SimpleMeanCalc();
		AbstractMeanCalc meanCalc = new StrictMeanCalc();
		
		for( double val : data) {
			System.out.println( "Input value: " + val);
			
			// 値を平均値計算器に入力
			meanCalc.addData( val);
			
			System.out.println( "Mean : " + meanCalc.calc());
		}
	}
}
